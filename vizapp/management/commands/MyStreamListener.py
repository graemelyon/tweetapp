from tweepy.streaming import StreamListener
from http.client import IncompleteRead

class MyStreamListener(StreamListener):
    def on_data(self, data):
        try:
            q.put(data)
            return True
        except IncompleteRead as e:
            print('Incomplete read occurred...')
            return False

    def on_error(self, status_code):
        print("The status code is {}.".format(status_code))
        if status_code == 420:
            print("Disconnecting stream...")
        return False

    def on_limit(self, track):
        print("Limitation notice arrived")
        return
