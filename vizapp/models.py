from django.db import models


class Topic(models.Model):
    topic_text = models.CharField(max_length=200)
    count = models.IntegerField(default=0)
    def __str__(self):
        return self.topic_text
